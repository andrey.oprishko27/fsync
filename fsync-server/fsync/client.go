package fsync

import (
	"context"
	"fmt"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/proto"
	"google.golang.org/grpc"
	"sync"
)

type SyncClients struct {
	clients []FileSyncClient
	mux     sync.Mutex
}

func InitClients() *SyncClients {
	return &SyncClients{clients: make([]FileSyncClient, 0)}
}

func (s *SyncClients) AddClient(c FileSyncClient) {
	s.mux.Lock()
	defer s.mux.Unlock()

	s.clients = append(s.clients, c)
}

func (s *SyncClients) GetClients() []FileSyncClient {
	s.mux.Lock()
	defer s.mux.Unlock()

	return s.clients
}

type FileSyncClient interface {
	Close() error
	ListFiles() (*sync_proto.FileStats, error)
	SyncFile(request *sync_proto.SyncRequest) (*sync_proto.SyncResponse, error)
	GetAddr() string
}

type syncClient struct {
	syncConn       *grpc.ClientConn
	grpcTimeoutSec int
	grpcClient     sync_proto.SyncServiceClient
	addr           string
}

func New(addr string, timeout int) (FileSyncClient, error) {

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("grpc Dial error: %s", err)
	}

	return &syncClient{addr: addr, syncConn: conn, grpcTimeoutSec: timeout, grpcClient: sync_proto.NewSyncServiceClient(conn)}, nil
}

func (s *syncClient) GetAddr() string {
	return s.addr
}

func (s *syncClient) ListFiles() (*sync_proto.FileStats, error) {
	return s.grpcClient.ListFiles(context.Background(), &sync_proto.Empty{})
}

func (s *syncClient) SyncFile(r *sync_proto.SyncRequest) (*sync_proto.SyncResponse, error) {
	return s.grpcClient.SyncFile(context.Background(), r)
}

func (s *syncClient) Close() error {
	return s.syncConn.Close()
}
