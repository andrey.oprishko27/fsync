package conf

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
)

// Config holds the app config.
type Config struct {
	AppID          string
	LogLevel       string
	GrpcTimeoutSec int
	HostPort       int
	SrcDirPath     string
	SyncTimeSec    int
	NumSyncWorkers int
}

const (
	appIDEnv          = "APP_ID"
	logLevelEnv       = "LOG_LEVEL"
	grpcTimeoutSecEnv = "GRPC_TIMEOUT_SEC"
	hostPortEnv       = "HOST_PORT"
	srcDirPathEnv     = "SRC_DIR_PATH"
	syncTimeSecEnv    = "SYNC_TIME_SEC"
	numSyncWorkersEnv = "NUM_SYNC_WORKERS"
)

// InitConf initializes a new config from a system env.
func InitConf() (*Config, error) {
	var err error
	grpcTimeoutSec, err := strconv.Atoi(os.Getenv(grpcTimeoutSecEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", grpcTimeoutSecEnv)
		return &Config{}, err
	}
	hostPort, err := strconv.Atoi(os.Getenv(hostPortEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", hostPortEnv)
		return &Config{}, err
	}
	syncTimeSec, err := strconv.Atoi(os.Getenv(syncTimeSecEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", syncTimeSecEnv)
		return &Config{}, err
	}
	numSyncWorkers, err := strconv.Atoi(os.Getenv(numSyncWorkersEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", numSyncWorkers)
		return &Config{}, err
	}
	var cfg = &Config{
		AppID:          os.Getenv(appIDEnv),
		LogLevel:       os.Getenv(logLevelEnv),
		GrpcTimeoutSec: grpcTimeoutSec,
		HostPort:       hostPort,
		SrcDirPath:     os.Getenv(srcDirPathEnv),
		SyncTimeSec:    syncTimeSec,
		NumSyncWorkers: numSyncWorkers,
	}
	err = cfg.Validate()

	return cfg, err
}

// Validate validates all Config fields.
func (cfg *Config) Validate() error {
	if cfg.AppID == "" {
		return fmt.Errorf("%s not set", appIDEnv)
	}

	if cfg.LogLevel == "" {
		return fmt.Errorf("%s not set", logLevelEnv)
	}

	if cfg.GrpcTimeoutSec == 0 {
		return fmt.Errorf("%s not set", grpcTimeoutSecEnv)
	}

	if cfg.HostPort == 0 {
		return fmt.Errorf("%s not set", hostPortEnv)
	}

	if cfg.SyncTimeSec == 0 {
		return fmt.Errorf("%s not set", syncTimeSecEnv)
	}

	if cfg.SrcDirPath == "" {
		return fmt.Errorf("%s not set", srcDirPathEnv)
	}

	if cfg.NumSyncWorkers == 0 {
		return fmt.Errorf("%s not set", numSyncWorkersEnv)
	}

	return nil
}
