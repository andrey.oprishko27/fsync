package conf

import (
	"os"

	log "github.com/sirupsen/logrus"
)

type formatter struct {
	log.Formatter
	defaultFields log.Fields
}

func (f *formatter) Format(entry *log.Entry) ([]byte, error) {
	for k, v := range f.defaultFields {
		entry.Data[k] = v
	}
	return f.Formatter.Format(entry)
}

func hostname() (hn string) {
	var err error
	if hn, err = os.Hostname(); err != nil {
		log.WithError(err).Fatal("Failed to get hostname")
	}
	return
}

// InitLogger is used for logging configurations
func InitLogger(cfg *Config) {
	log.SetFormatter(&formatter{
		Formatter: &log.JSONFormatter{},
		defaultFields: log.Fields{
			"service": cfg.AppID,
			"host":    hostname(),
		},
	})
	if l, err := log.ParseLevel(cfg.LogLevel); err == nil {
		log.SetLevel(l)
	} else {
		log.Panic(err)
	}
}
