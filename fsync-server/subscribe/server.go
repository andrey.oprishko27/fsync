package subscribe

import (
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/sirupsen/logrus"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/proto"
	"google.golang.org/grpc"
	"net"
)

type subServer struct {
	port int
	srv  sync_proto.SubscribeServiceServer
}

func NewSubGrpcServer(port int, srv sync_proto.SubscribeServiceServer) *subServer {
	return &subServer{
		port: port,
		srv:  srv,
	}
}

func (s *subServer) Start() {
	errs := make(chan error, 1)

	logger := logrus.WithField("service", "subscribe")

	grpcListener, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}

	logrus.WithFields(logrus.Fields{
		"component": "gRPC",
		"port":      s.port,
	}).Info("listening...")

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_logrus.StreamServerInterceptor(logger),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_logrus.UnaryServerInterceptor(logger),
		)),
	)

	sync_proto.RegisterSubscribeServiceServer(grpcServer, s.srv)

	errs <- grpcServer.Serve(grpcListener)

	logrus.Fatal("terminated", errs)
}

func (s *subServer) Stop() {}

func (s *subServer) Title() string {
	return "grpc subscribe srv"
}
