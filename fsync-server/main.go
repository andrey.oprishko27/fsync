package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/conf"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/service"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/subscribe"
	"os"
	"os/signal"
)

// Module base module interface
type Module interface {
	Start()
	Stop()
	Title() string
}

func main() {
	cfg, err := conf.InitConf()
	if err != nil {
		log.Fatalf("Cannot decode config: %s", err.Error())
	}

	conf.InitLogger(cfg)

	// init service
	srv, err := service.New(cfg.GrpcTimeoutSec, cfg.SyncTimeSec, cfg.SrcDirPath, cfg.NumSyncWorkers)

	if err != nil {
		log.WithError(err).Fatal("cannot initialize service")
	}

	// init subscribe grpc service
	subServer := subscribe.NewSubGrpcServer(cfg.HostPort, srv)

	runModules(subServer, srv)
}

// runModules runs each of the modules in a separate goroutine.
func runModules(modules ...Module) {
	if len(modules) > 0 {
		for _, m := range modules {
			log.Infof("Starting module %s", m.Title())
			go m.Start()
		}

		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c

		for _, m := range modules {
			log.Infof("Stopping module %s", m.Title())
			m.Stop()
		}

		log.Infof("Stopped all modules")
	}
}
