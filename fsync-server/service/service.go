package service

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/fsync"
	"gitlab.com/andrey.oprishko27/fsync/fsync-server/proto"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

type srv struct {
	clients        *fsync.SyncClients
	grpcTimeoutSec int
	syncTimeoutSec int
	syncDirPath    string
	stop           chan bool
	numSyncWorkers int
	localFileStats *sync_proto.FileStats
}

// New constructor for service
func New(grpcTimeoutSec, syncTimeoutSec int, syncDirPath string, numSyncWorkers int) (*srv, error) {
	srv := &srv{
		grpcTimeoutSec: grpcTimeoutSec,
		syncTimeoutSec: syncTimeoutSec,
		syncDirPath:    syncDirPath,
		stop:           make(chan bool),
		numSyncWorkers: numSyncWorkers,
		clients:        fsync.InitClients(),
	}

	err := srv.listLocalFiles()
	if err != nil {
		return nil, fmt.Errorf("error while getting file stats. %s", err)
	}

	return srv, nil
}

func (s *srv) GetLocalFileStats() *sync_proto.FileStats {
	stats := &sync_proto.FileStats{Files: make(map[string]*sync_proto.FileStat, len(s.localFileStats.Files))}

	for name, st := range s.localFileStats.Files {
		stats.Files[name] = &sync_proto.FileStat{Name: st.Name, Hash: st.Hash}
	}

	return stats
}

// SubForFileUpdate implements subscribing clients for file syncing
func (s *srv) SubForFileUpdate(ctx context.Context, req *sync_proto.SubRequest) (*sync_proto.SubResponse, error) {
	resp := &sync_proto.SubResponse{}
	c, err := fsync.New(req.SyncAddr, s.grpcTimeoutSec)
	if err != nil {
		return resp, err
	}

	s.clients.AddClient(c)

	resp.Ok = true
	return resp, nil
}

func (s *srv) Start() {
	ticker := time.NewTicker(time.Duration(s.syncTimeoutSec) * time.Second)
	defer ticker.Stop()

	go func() {
		for range ticker.C {
			logrus.Infof("start syncing dirs")
			err := s.syncDispatcher()

			if err != nil {
				logrus.WithError(err).Error("cannt sync files")
			}
		}
	}()

	<-s.stop
}

func (s *srv) syncDispatcher() error {
	err := s.listLocalFiles()
	if err != nil {
		return err
	}

	clients := s.clients.GetClients()

	clientJobs := make(chan fsync.FileSyncClient, len(clients))
	results := make(chan Statistic, len(clients))

	for w := 0; w < s.numSyncWorkers; w++ {
		go s.syncFiles(clientJobs, results)
	}

	for _, c := range clients {
		clientJobs <- c
	}
	close(clientJobs)

	for i := 0; i < len(clients); i++ {
		stat := <-results

		logrus.WithFields(logrus.Fields{
			"client addr":           stat.ClientAddr,
			"sync time utc":         stat.StartSync,
			"total files on server": stat.TotalFilesOnServer,
			"updated on client":     stat.UpdatedOnClient,
			"actual on client":      stat.ActualFilesOnClient,
		}).Info("sync stats")
	}

	return nil
}

type Statistic struct {
	ClientAddr          string
	StartSync           time.Time
	TotalFilesOnServer  int
	UpdatedOnClient     int
	ActualFilesOnClient int
}

func (s *srv) syncFiles(clients <-chan fsync.FileSyncClient, result chan<- Statistic) {

	for c := range clients {
		stats := Statistic{
			ClientAddr:         c.GetAddr(),
			StartSync:          time.Now().UTC(),
			TotalFilesOnServer: len(s.localFileStats.Files),
		}

		clientStats, err := c.ListFiles()
		if err != nil {
			logrus.WithError(err).Errorf("cannot get list files from client %s", c.GetAddr())
			result <- stats
			continue
		}
		logrus.Infof("successfully get list from client %s", c.GetAddr())

		reqs, numActualFiles := s.generateRequests(clientStats)
		stats.ActualFilesOnClient = numActualFiles

		for _, req := range reqs {

			resp, err := c.SyncFile(req)
			if err != nil || !resp.Ok {
				if resp != nil {
					logrus.WithError(err).Infof("error sync file. %s", resp.Message)
				} else {
					logrus.WithError(err).Info("error sync file")
				}

				result <- stats
				continue
			}

			stats.UpdatedOnClient++

			logrus.Infof("client %s successfully update files", c.GetAddr())

		}

		result <- stats
	}
}

// returns requests and number of actual files on client
func (s *srv) generateRequests(cStats *sync_proto.FileStats) ([]*sync_proto.SyncRequest, int) {
	var (
		req            = make([]*sync_proto.SyncRequest, 0, len(cStats.Files))
		numActualFiles int
	)
	localStats := s.GetLocalFileStats()

	for name, st := range cStats.Files {
		if lst, ok := localStats.Files[name]; ok {
			// if file exits no sync
			if lst.Hash == st.Hash {
				delete(localStats.Files, name)
				numActualFiles++
				continue
			}
		}

		// if locally file not exits action delete
		r := &sync_proto.SyncRequest{SyncAction: sync_proto.Action_DELETE, FileStat: st}
		req = append(req, r)
	}

	// add all files, which don't exits
	for name, st := range localStats.Files {
		data, err := s.getFileBody(name)
		if err != nil {
			logrus.Errorf("cannot add file for sync. error while read body of file %s. %s", name)
		}

		r := &sync_proto.SyncRequest{SyncAction: sync_proto.Action_PUT, FileStat: st, Data: data}
		req = append(req, r)
	}

	return req, numActualFiles
}

func (s *srv) getFileBody(filePath string) ([]byte, error) {
	body, err := ioutil.ReadFile(filePath)
	if err != nil {
		return make([]byte, 0), err
	}

	return body, nil
}

func (s *srv) listLocalFiles() error {
	stats := &sync_proto.FileStats{
		Files: make(map[string]*sync_proto.FileStat, 10),
	}

	err := filepath.Walk(s.syncDirPath,
		func(path string, info os.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}

			logrus.Infof("processing file %s", path)
			if err != nil {
				return fmt.Errorf("cannot get file %s. %s", path, err)
			}

			hash, err := hashFileMd5(path)
			if err != nil {
				return fmt.Errorf("cannot get hash file %s. %s", path, err)
			}

			st := &sync_proto.FileStat{
				Name: path,
				Hash: hash,
			}

			stats.Files[path] = st
			return nil
		})
	if err != nil {
		return fmt.Errorf("cannot get file info. %s", err)
	}

	s.localFileStats = stats
	return nil
}

func hashFileMd5(filePath string) (string, error) {
	var returnMD5String string
	file, err := os.Open(filePath)
	if err != nil {
		return returnMD5String, err
	}
	defer file.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return returnMD5String, err
	}
	hashInBytes := hash.Sum(nil)[:16]
	returnMD5String = hex.EncodeToString(hashInBytes)
	return returnMD5String, nil
}

func (s *srv) Stop() {
	s.stop <- true
	defer close(s.stop)
}

func (s *srv) Title() string {
	return "file sync server"
}
