module gitlab.com/andrey.oprishko27/fsync/fsync-server

require (
	github.com/gogo/protobuf v1.2.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/sirupsen/logrus v1.4.0
	google.golang.org/grpc v1.19.0
)
