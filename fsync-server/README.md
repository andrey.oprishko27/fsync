# fsync server
## Envs:
```$xslt
# app id inditificates service
APP_ID=fsync_server
# Log level
LOG_LEVEL=DEBUG
# timeout for grpc in sec
GRPC_TIMEOUT_SEC=10
# host port for grpc server
HOST_PORT=1010
# source dir for file storing
SRC_DIR_PATH=./src
# time for syncing files in folder
SYNC_TIME_SEC=10
# num goroutines to process sync files
NUM_SYNC_WORKERS=10
```

## How it works
```$xslt
Defined diff of local and remote (clients) src and make requests to update state on clients.
```
