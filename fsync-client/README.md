# fsync client
## Envs:
```$xslt
# app id inditificates service
APP_ID=fsync_client
# log level
LOG_LEVEL=DEBUG
# timeout for grpc in sec
GRPC_TIMEOUT_SEC=10
# host port for grpc server
HOST_ADDR=1111
# app addr for server subscrition
CONNECTION_ADDR=localhost:1111
# fsync server app addr
SERVER_ADDR=localhost:1010
# time for syncing files in folder
SRC_DIR_PATH=./src
# time
RESUBSRIBE_SEC=10
```
## How it works
```$xslt
When client stats it tries to subscribe to file sync server by addr defined in envs.
In env also defines resubscribing time.

There is grpc service for handling file sync local src
```

