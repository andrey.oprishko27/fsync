package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/conf"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/fsync"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/service"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/subscribe"
	"os"
	"os/signal"
	"time"
)

// Module base module interface
type Module interface {
	Start()
	Stop()
	Title() string
}

func main() {
	cfg, err := conf.InitConf()
	if err != nil {
		log.Fatalf("Cannot decode config: %s", err)
	}

	conf.InitLogger(cfg)

	subClient, err := subscribe.New(cfg.ServerAddr, cfg.GrpcTimeoutSec)
	if err != nil {
		log.WithError(err).Fatal("Cannot init subscription grpc client")
	}

	// initialize grcp sync service
	srv := service.New(cfg.SrcDirPath)
	syncSrv := fsync.NewSyncGrpcServer(cfg.HostPort, srv)

	go func() {
		ticker := time.NewTicker(time.Duration(cfg.ResubscribeSec) * time.Second)
		defer ticker.Stop()

		for range ticker.C {
			// Subscribe for file update
			r, err := subClient.SubForFileUpdate(cfg.ConnectionAddr)
			if err != nil {
				log.WithError(err).Error("Cannot subscribe for file updates")
				continue
			}

			// Warn for already subscribed clients
			if !r.Ok {
				log.Warning("already subscribed for file updates")
			}
		}
	}()

	log.Info("successfully subscribed for file updates")

	runModules(syncSrv)
}

// runModules runs each of the modules in a separate goroutine.
func runModules(modules ...Module) {
	if len(modules) > 0 {
		for _, m := range modules {
			log.Infof("Starting module %s", m.Title())
			go m.Start()
		}

		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c

		for _, m := range modules {
			log.Infof("Stopping module %s", m.Title())
			m.Stop()
		}

		log.Infof("Stopped all modules")
	}
}
