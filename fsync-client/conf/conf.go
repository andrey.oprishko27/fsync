package conf

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
)

// Config holds the app config.
type Config struct {
	AppID          string
	LogLevel       string
	GrpcTimeoutSec int
	ConnectionAddr string
	HostPort       int
	ServerAddr     string
	SrcDirPath     string
	ResubscribeSec int
}

const (
	appIDEnv          = "APP_ID"
	logLevelEnv       = "LOG_LEVEL"
	grpcTimeoutSecEnv = "GRPC_TIMEOUT_SEC"
	hostPortEnv       = "HOST_ADDR"
	connectionAddrEnv = "CONNECTION_ADDR"
	serverAddrEnv     = "SERVER_ADDR"
	srcDirPathEnv     = "SRC_DIR_PATH"
	resubscribeSecEnv = "RESUBSRIBE_SEC"
)

// InitConf initializes a new config from a system env.
func InitConf() (*Config, error) {
	var err error
	grpcTimeoutSec, err := strconv.Atoi(os.Getenv(grpcTimeoutSecEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", grpcTimeoutSecEnv)
		return &Config{}, err
	}
	hostPort, err := strconv.Atoi(os.Getenv(hostPortEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", hostPortEnv)
		return &Config{}, err
	}
	resubSec, err := strconv.Atoi(os.Getenv(resubscribeSecEnv))
	if err != nil {
		logrus.WithError(err).Errorf("%s must be int", resubscribeSecEnv)
		return &Config{}, err
	}

	var cfg = &Config{
		AppID:          os.Getenv(appIDEnv),
		LogLevel:       os.Getenv(logLevelEnv),
		GrpcTimeoutSec: grpcTimeoutSec,
		ConnectionAddr: os.Getenv(connectionAddrEnv),
		HostPort:       hostPort,
		ServerAddr:     os.Getenv(serverAddrEnv),
		SrcDirPath:     os.Getenv(srcDirPathEnv),
		ResubscribeSec: resubSec,
	}
	err = cfg.Validate()

	return cfg, err
}

// Validate validates all Config fields.
func (cfg *Config) Validate() error {
	if cfg.AppID == "" {
		return fmt.Errorf("%s not set", appIDEnv)
	}
	if cfg.LogLevel == "" {
		return fmt.Errorf("%s not set", logLevelEnv)
	}
	if cfg.GrpcTimeoutSec == 0 {
		return fmt.Errorf("%s not set", grpcTimeoutSecEnv)
	}
	if cfg.ConnectionAddr == "" {
		return fmt.Errorf("%s not set", connectionAddrEnv)
	}
	if cfg.HostPort == 0 {
		return fmt.Errorf("%s not set", hostPortEnv)
	}
	if cfg.ServerAddr == "" {
		return fmt.Errorf("%s not set", serverAddrEnv)
	}
	if cfg.SrcDirPath == "" {
		return fmt.Errorf("%s not set", srcDirPathEnv)
	}
	if cfg.ResubscribeSec == 0 {
		return fmt.Errorf("%s not set", resubscribeSecEnv)
	}

	return nil
}
