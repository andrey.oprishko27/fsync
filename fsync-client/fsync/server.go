package fsync

import (
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/sirupsen/logrus"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/proto"
	"google.golang.org/grpc"
	"net"
)

type syncServer struct {
	port int
	srv  sync_proto.SyncServiceServer
}

func NewSyncGrpcServer(port int, srv sync_proto.SyncServiceServer) *syncServer {
	return &syncServer{
		port: port,
		srv:  srv,
	}
}

// Start serving grpc file sync server
func (s *syncServer) Start() {
	errs := make(chan error, 1)

	logger := logrus.WithField("service", "sync")

	grpcListener, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}

	logrus.WithFields(logrus.Fields{
		"component": "gRPC",
		"port":      s.port,
	}).Info("listening...")

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_logrus.StreamServerInterceptor(logger),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_logrus.UnaryServerInterceptor(logger),
		)),
	)

	sync_proto.RegisterSyncServiceServer(grpcServer, s.srv)

	errs <- grpcServer.Serve(grpcListener)

	logrus.Fatal("terminated", errs)
}

// Stop .
func (s *syncServer) Stop() {}

// Title .
func (s *syncServer) Title() string {
	return "grpc sync srv"
}
