#!/bin/bash


# go get github.com/gogo/protobuf/{proto,protoc-gen-gogo,gogoproto,protoc-gen-gofast,protoc-gen-gogofaster}

protoc \
	--proto_path=$GOPATH/src \
	--proto_path=$GOPATH/src/github.com/gogo/protobuf/protobuf \
		--gogofaster_out=plugins=grpc,Mgoogle/api/annotations.proto=github.com/gogo/googleapis/google/api:./proto/ \
	--proto_path=./proto/ \
	./proto/*.proto