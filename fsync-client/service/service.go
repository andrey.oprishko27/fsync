package service

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/proto"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

type SyncService interface {
	ListFiles(ctx context.Context, r *sync_proto.Empty) (*sync_proto.FileStats, error)
	SyncFile(ctx context.Context, r *sync_proto.SyncRequest) (*sync_proto.SyncResponse, error)
}

type srv struct {
	srcDir string
}

func New(srcDir string) *srv {
	return &srv{srcDir: srcDir}
}

func (s *srv) ListFiles(ctx context.Context, r *sync_proto.Empty) (*sync_proto.FileStats, error) {
	stats := &sync_proto.FileStats{
		Files: make(map[string]*sync_proto.FileStat, 10),
	}

	err := filepath.Walk(s.srcDir,
		func(path string, info os.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}
			if err != nil {
				return fmt.Errorf("cannot get file %s. %s", path, err)
			}

			hash, err := hashFileMd5(path)
			if err != nil {
				return fmt.Errorf("cannot get hash file %s. %s", path, err)
			}

			st := &sync_proto.FileStat{
				Name: path,
				Hash: hash,
			}

			stats.Files[path] = st
			return nil
		})
	if err != nil {
		logrus.WithError(err).Info("cannot get file info")
	}

	logrus.Info("file stats successfully listed")

	return stats, err
}

func hashFileMd5(filePath string) (string, error) {
	var returnMD5String string
	file, err := os.Open(filePath)
	if err != nil {
		return returnMD5String, err
	}
	defer file.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return returnMD5String, err
	}
	hashInBytes := hash.Sum(nil)[:16]
	returnMD5String = hex.EncodeToString(hashInBytes)
	return returnMD5String, nil

}

func (s *srv) SyncFile(ctx context.Context, r *sync_proto.SyncRequest) (*sync_proto.SyncResponse, error) {
	logrus.WithFields(logrus.Fields{
		"action":    r.SyncAction,
		"file name": r.FileStat.Name,
		"hash":      r.FileStat.Hash,
	}).Info("start file sync")
	var (
		resp = &sync_proto.SyncResponse{}
		err  error
	)

	switch r.SyncAction {
	case sync_proto.Action_DELETE:
		err = os.Remove(r.FileStat.Name)
		if err == nil {
			resp.Ok = true
		} else {
			err = fmt.Errorf("cannot remove file %s. %s", r.FileStat.Name, err)
			resp.Message = err.Error()
		}
	case sync_proto.Action_PUT:
		err := ioutil.WriteFile(r.FileStat.Name, r.Data, 0644)
		if err == nil {
			resp.Ok = true
		} else {
			err = fmt.Errorf("cannot wright file %s. %s", r.FileStat.Name, err)
			resp.Message = err.Error()
		}
	}

	logrus.WithField("file name", r.FileStat.Name).Info("file successfully updated")

	return resp, err
}
