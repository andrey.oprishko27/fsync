package subscribe

import (
	"context"
	"fmt"
	"gitlab.com/andrey.oprishko27/fsync/fsync-client/proto"
	"google.golang.org/grpc"
)

type SyncClient interface {
	Close() error
	SubForFileUpdate(appAdd string) (*sync_proto.SubResponse, error)
}

type subClient struct {
	subConn        *grpc.ClientConn
	grpcTimeoutSec int
	grpcClient     sync_proto.SubscribeServiceClient
}

func New(addr string, timeout int) (SyncClient, error) {

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("grpc Dial error: %s", err)
	}

	return &subClient{subConn: conn, grpcTimeoutSec: timeout, grpcClient: sync_proto.NewSubscribeServiceClient(conn)}, nil
}

func (s *subClient) SubForFileUpdate(appAddr string) (*sync_proto.SubResponse, error) {
	return s.grpcClient.SubForFileUpdate(context.Background(), &sync_proto.SubRequest{SyncAddr: appAddr})
}

func (s *subClient) Close() error {
	return s.subConn.Close()
}
